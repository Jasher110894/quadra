# -*- coding: utf-8 -*-
from django.db import models

class Post(models.Model):
    datetime = models.DateTimeField(u'Дата публикации', null=True)
    content = models.TextField(max_length=10000)

    def __unicode__(self):
        return self.content

    def get_absolute_url(self):
        return "/blog/%i/" % self.id