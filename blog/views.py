# -*- coding: utf-8 -*-

from blog.models import Post 
from django.views.generic import ListView, DetailView
from django.shortcuts import render, HttpResponse, HttpResponseRedirect


def homepage(request):
	template = "index.html"

	return render(request, template)

class PostsListView(ListView):
    model = Post                  

class PostDetailView(DetailView):
    model = Post
