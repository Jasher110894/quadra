#coding: utf-8
from django.conf.urls import patterns, url, include

from blog.views import PostsListView, PostDetailView 

urlpatterns = patterns('blog.views',
	url(regex = r'^$', view = 'homepage', name = 'homepage'),

)