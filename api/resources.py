import urlparse
from tastypie.serializers import Serializer
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource
from blog.models import Post


class urlencodeSerializer(Serializer):
    formats = ['json', 'jsonp', 'xml', 'yaml', 'html', 'plist', 'urlencode']
    content_types = {
        'json': 'application/json',
        'jsonp': 'text/javascript',
        'xml': 'application/xml',
        'yaml': 'text/yaml',
        'html': 'text/html',
        'plist': 'application/x-plist',
        'urlencode': 'application/x-www-form-urlencoded',
        }
    def from_urlencode(self, data,options=None):
        """ handles basic formencoded url posts """
        qs = dict((k, v if len(v)>1 else v[0] )
            for k, v in urlparse.parse_qs(data).iteritems())
        return qs

    def to_urlencode(self,content): 
        pass

class PostResource(ModelResource):
    class Meta:
    	serializer = urlencodeSerializer()
    	authorization= Authorization()
        queryset = Post.objects.all()
        resource_name = 'post'
        allowed_methods = ['get', 'post', 'put']
